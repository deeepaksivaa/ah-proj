import React, { Component } from "react";
import { useState, useEffect } from "react";
import {
    Container,
    Row,
    Col,
    Table,
    ButtonGroup,
    Button,
    Radio,
} from "bootstrap-4-react";

import DataTable from "./DataTable";

export default function App() {
    const [entries, setEntries] = useState(10);
    const [searchIndex, setSearchIndex] = useState("");
    const [minRange, SetMinRange] = useState(0);
    const [maxRange, SetMaxRange] = useState(1000);

    return (
        <Container>
            <Row>
                <Col col="lg">
                    <div className="text-center heading">
                        <h1>Electric Vehicle Details</h1>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col col="lg">
                    <div>
                        <span className="entriesText">No of Entries</span>
                        <span>
                            <input
                                type="number"
                                value={entries}
                                onChange={(e) => setEntries(e.target.value)}
                            />
                        </span>
                    </div>
                </Col>
                <Col col="lg" justifyContent="end">
                    <div>
                        <div>
                            <span className="searchText">Search</span>
                            <span>
                                <input
                                    type="text"
                                    value={searchIndex}
                                    onChange={(e) =>
                                        setSearchIndex(e.target.value)
                                    }
                                />
                            </span>
                        </div>
                        <div>
                            <span className="erText">Electric Range</span>
                            <span>
                                <input
                                    className="erInput"
                                    type="number"
                                    value={minRange}
                                    onChange={(e) =>
                                        SetMinRange(e.target.value)
                                    }
                                />
                            </span>
                            <span>
                                <input
                                    type="number"
                                    value={maxRange}
                                    onChange={(e) =>
                                        SetMaxRange(e.target.value)
                                    }
                                />
                            </span>
                        </div>
                        <div>
                            <Button
                                variant="primary"
                                size="sm"
                                onClick={() =>
                                    SetMinRange(0) && SetMaxRange(1000)
                                }
                            >
                                Clear Filters
                            </Button>
                        </div>
                    </div>
                </Col>
            </Row>
            <Row className="dataTable">
                <DataTable
                    Entries={entries}
                    SearchIndex={searchIndex}
                    Min={minRange}
                    Max={maxRange}
                />
            </Row>
        </Container>
    );
}
