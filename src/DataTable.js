import React, { useState, useEffect } from "react";
import { Table, Row, Col } from "bootstrap-4-react";

import JSONDATA from "./data.json";

function DataTable({ Entries, SearchIndex, Min, Max }) {
    const [filteredDatas, setFilteredDatas] = useState([]);
    const [electricUtility, setElectricUtility] = useState(0);
    const eu = 0;

    useEffect(() => {
        getFilterDatas();
    });

    const getFilterDatas = () => {
        let renderDatas = JSONDATA.slice(0, Entries).filter((val) => {
            if (SearchIndex === "") {
                return val;
            } else if (
                val.make.toLowerCase().includes(SearchIndex.toLowerCase()) &&
                val.electric_range >= Min &&
                val.electric_range <= Max
            ) {
                return val;
            } else {
                return "";
            }
        });

        setFilteredDatas(renderDatas);
    };

    return (
        <div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>VIN (1-10)</th>
                        <th>Model Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Electric Vehicle Type</th>
                        <th>Electric Range</th>
                        <th>Electric Utility</th>
                        <th>Tax</th>
                        <th>Insurance</th>
                        <th>Delivery Charges</th>
                        <th>Extra Charges</th>
                        <th>Show All</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredDatas.map((data, index) => (
                        <tr key={index}>
                            <td>{data.vin}</td>
                            <td>{data.model_year}</td>
                            <td>{data.make}</td>
                            <td>{data.model}</td>
                            <td>{data.electric_vehicle_type}</td>
                            <td>{data.electric_range}</td>
                            <td>{data.electric_utility}</td>
                            <td>{data.tax}</td>
                            <td>{data.insurance}</td>
                            <td>{data.delivery_charges}</td>
                            <td>{data.extra_charges}</td>
                            <td>....</td>
                        </tr>
                    ))}
                </tbody>
            </Table>

            <Row>
                <Col col="4">
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Model Year</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filteredDatas
                                    .slice(0, 3)
                                    .map((data, index) => (
                                        <tr key={index}>
                                            <td>{data.model_year}</td>
                                            <td>{data.make}</td>
                                            <td>{data.model}</td>
                                            <td>12</td>
                                        </tr>
                                    ))}
                            </tbody>
                        </Table>
                    </div>
                    <div>
                        <Table striped bordered hover className="countryTable">
                            <thead>
                                <tr>
                                    <th>Country</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filteredDatas
                                    .slice(0, 3)
                                    .map((data, index) => (
                                        <tr key={index}>
                                            <td>{data.country}</td>
                                            <td>12</td>
                                        </tr>
                                    ))}
                            </tbody>
                        </Table>
                    </div>
                </Col>
                <Col col="8">
                    <iframe
                        class="map"
                        width="720"
                        height="460"
                        frameborder="0"
                        scrolling="no"
                        marginheight="0"
                        marginwidth="0"
                        src="https://maps.google.com/?ie=UTF8&t=m&ll=13.2164639,74.995161&spn=0.003381,0.017231&z=16&output=embed"
                    ></iframe>
                </Col>
            </Row>
        </div>
    );
}

export default DataTable;
